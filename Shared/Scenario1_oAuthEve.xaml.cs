﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using EveSSO;
using System;
using System.Linq;
using System.Collections.Generic;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.Security.Authentication.Web;
using Windows.Security.Cryptography.Core;
using Windows.Security.Cryptography;
using Windows.Storage.Streams;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using Windows.ApplicationModel.Activation;
using Windows.Data.Json;

namespace WebAuthentication
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
#if WINDOWS_PHONE_APP
    public sealed partial class Scenario1 : Page, IWebAuthenticationContinuable
#else
    public sealed partial class Scenario1 : Page  
#endif  
    {
        double expires_in = 0;
        string access_token = null;
        string token_type = null;
        string refresh_token = null;
        
        // A pointer back to the main page.  This is needed if you want to call methods in MainPage such
        // as NotifyUser()
        MainPage rootPage = MainPage.Current;

        public Scenario1()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private async Task<String> SendDataAsync(String Url)
        {
            try
            {
                HttpClient httpClient = new HttpClient();
                return await httpClient.GetStringAsync(new Uri(Url));
            }
            catch (Exception Err)
            {
                rootPage.NotifyUser("Error getting data from server." + Err.Message, NotifyType.StatusMessage);
            }

            return null;
        }

        private void DebugPrint(String Trace)
        {
            EveDebugArea.Text += Trace + "\r\n";
        }

        private void OutputToken(String TokenUri)
        {
            EveReturnedToken.Text = TokenUri;
        }

#if WINDOWS_PHONE_APP
        private async void Launch_Click(object sender, RoutedEventArgs e)
#else
        private async void Launch_Click(object sender, RoutedEventArgs e)
#endif 
        {
            if (EveClientID.Text == "")
            {
                rootPage.NotifyUser("Please enter an Client ID.", NotifyType.StatusMessage);
            }
            else if (EveCallbackUrl.Text == "")
            {
                EveCallbackUrl.Text = WebAuthenticationBroker.GetCurrentApplicationCallbackUri().ToString();
            }
            else if (EveClientSecret.Text == "")
            {
                rootPage.NotifyUser("Please enter an Client Secret.", NotifyType.StatusMessage);
            }

            try
            {
                string callbackUrl = WebAuthenticationBroker.GetCurrentApplicationCallbackUri().ToString();
                DebugPrint("Application callback URI is: " + EveCallbackUrl.Text);
                //string EveUrl = string.Format("https://login.eveonline.com/oauth/authorize/?response_type=code&redirect_uri={0}&client_id={1}&scope=publicData characterStatisticsRead&state=uniquestate123",
                //    EveCallbackUrl.Text, EveClientID.Text);
                string EveUrl = string.Format("https://sisilogin.testeveonline.com/oauth/authorize/?response_type=code&redirect_uri={0}&client_id={1}&scope=publicData characterStatisticsRead&state=uniquestate123",
                    EveCallbackUrl.Text, EveClientID.Text);
                System.Uri StartUri = new Uri(EveUrl);

                DebugPrint("Navigating to: " + EveUrl);
#if WINDOWS_PHONE_APP
                WebAuthenticationBroker.AuthenticateAndContinue(StartUri);

#else
                WebAuthenticationResult WebAuthenticationResult = await WebAuthenticationBroker.AuthenticateAsync(
                                                        WebAuthenticationOptions.None,
                                                        StartUri);
                if (WebAuthenticationResult.ResponseStatus == WebAuthenticationStatus.Success)
                {
                    OutputToken(WebAuthenticationResult.ResponseData.ToString());
                    await GetEveUserNameAsync(WebAuthenticationResult.ResponseData.ToString());
                }
                else if (WebAuthenticationResult.ResponseStatus == WebAuthenticationStatus.ErrorHttp)
                {
                    OutputToken("HTTP Error returned by AuthenticateAsync() : " + WebAuthenticationResult.ResponseErrorDetail.ToString());
                }
                else
                {
                    OutputToken("Error returned by AuthenticateAsync() : " + WebAuthenticationResult.ResponseStatus.ToString());
                }

#endif
            }
            catch (Exception Error)
            {
                //
                // Bad Parameter, SSL/TLS Errors and Network Unavailable errors are to be handled here.
                //
                DebugPrint(Error.ToString());
            }
        }

#if WINDOWS_PHONE_APP
        public async void ContinueWebAuthentication(WebAuthenticationBrokerContinuationEventArgs args)
        {
            WebAuthenticationResult result = args.WebAuthenticationResult;


            if (result.ResponseStatus == WebAuthenticationStatus.Success)
            {
                OutputToken(result.ResponseData.ToString());
                await GetEveUserNameAsync(result.ResponseData.ToString());
            }
            else if (result.ResponseStatus == WebAuthenticationStatus.ErrorHttp)
            {
                OutputToken("HTTP Error returned by AuthenticateAsync() : " + result.ResponseErrorDetail.ToString());
            }
            else
            {
                OutputToken("Error returned by AuthenticateAsync() : " + result.ResponseStatus.ToString());
            }
        }
#endif

        private async Task GetEveUserNameAsync(string webAuthResultResponseData)
        {
            string returned_state = null;
            string auth_code = null;
            
            string[] splitURL = webAuthResultResponseData.Split('?');
            if( splitURL[0] == EveCallbackUrl.Text && splitURL.Length == 2 )
            {

                string[] keyValPairs = splitURL[1].Split('&');

                for (int i = 0; i < keyValPairs.Length; i++)
                {
                    string[] splits = keyValPairs[i].Split('=');
                    switch (splits[0])
                    {
                        case "code":
                            auth_code = splits[1];
                            break;
                        case "state":
                            returned_state = splits[1];
                            break;
                    }
                }
            }

            // TODO : Check returned state

            if (string.IsNullOrWhiteSpace(auth_code) == false)
            {
                //string EveUrl = "https://login.eveonline.com/oauth/token";
                string EveUrl = "https://sisilogin.testeveonline.com/oauth/token";

                HttpStringContent httpContent = new HttpStringContent("grant_type=authorization_code&code=" + auth_code, 
                    Windows.Storage.Streams.UnicodeEncoding.Utf8);
                httpContent.Headers.ContentType = HttpMediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");

                HttpClient httpClient = new HttpClient();

                IBuffer authBuffer = CryptographicBuffer.ConvertStringToBinary(string.Format("{0}:{1}",
                    EveClientID.Text, EveClientSecret.Text), BinaryStringEncoding.Utf8);
                string authHeaderParams = CryptographicBuffer.EncodeToBase64String(authBuffer);
                httpClient.DefaultRequestHeaders.Authorization = new HttpCredentialsHeaderValue("Basic", authHeaderParams);
                
                var httpResponseMessage = await httpClient.PostAsync(new Uri(EveUrl), httpContent);
                string response = await httpResponseMessage.Content.ReadAsStringAsync();

                JsonValue jsonValue = JsonValue.Parse(response);
                if( jsonValue.ValueType == JsonValueType.Object )
                {
                    JsonObject jsonObject = jsonValue.GetObject();
                    JsonValue tmpValue = jsonObject.GetNamedValue("expires_in");
                    if( tmpValue.ValueType == JsonValueType.Number )
                    {
                        expires_in = tmpValue.GetNumber();
                    }
                    tmpValue = jsonObject.GetNamedValue("access_token");
                    if (tmpValue.ValueType == JsonValueType.String)
                    {
                        access_token = tmpValue.GetString();
                    }
                    tmpValue = jsonObject.GetNamedValue("token_type");
                    if (tmpValue.ValueType == JsonValueType.String)
                    {
                        token_type = tmpValue.GetString();
                    }
                    tmpValue = jsonObject.GetNamedValue("refresh_token");
                    if (tmpValue.ValueType == JsonValueType.String)
                    {
                        refresh_token = tmpValue.GetString();
                    }
                }

                if (access_token != null)
                {
                    DebugPrint("access_token = " + access_token);
                }
                if (token_type != null)
                {
                    DebugPrint("token_type = " + token_type);
                }
                if (expires_in != null)
                {
                    DebugPrint("expires_in = " + expires_in);
                }
                if (refresh_token != null)
                {
                    DebugPrint("refresh_token = " + refresh_token);
                }
            }

            if (string.IsNullOrWhiteSpace(token_type) == false && string.IsNullOrWhiteSpace(access_token) == false)
            {
                //string EveUrl = "https://login.eveonline.com/oauth/verify";
                string EveUrl = "https://sisilogin.testeveonline.com/oauth/verify";

                HttpClient httpClient = new HttpClient();

                httpClient.DefaultRequestHeaders.Authorization = new HttpCredentialsHeaderValue(token_type, access_token);

                string response = await httpClient.GetStringAsync(new Uri(EveUrl));

                JsonValue jsonValue = JsonValue.Parse(response);
                if (jsonValue.ValueType == JsonValueType.Object)
                {
                    JsonObject jsonObject = jsonValue.GetObject();
                    JsonValue tmpValue = jsonObject.GetNamedValue("CharacterID");
                    if (tmpValue.ValueType == JsonValueType.String)
                    {
                        DebugPrint("CharacterID = " + tmpValue.GetString());
                    }
                    tmpValue = jsonObject.GetNamedValue("CharacterName");
                    if (tmpValue.ValueType == JsonValueType.String)
                    {
                        DebugPrint("CharacterName = " + tmpValue.GetString());
                    }
                    tmpValue = jsonObject.GetNamedValue("ExpiresOn");
                    if (tmpValue.ValueType == JsonValueType.String)
                    {
                        DebugPrint("ExpiresOn = " + tmpValue.GetString());
                    }
                    tmpValue = jsonObject.GetNamedValue("Scopes");
                    if (tmpValue.ValueType == JsonValueType.String)
                    {
                        DebugPrint("Scopes = " + tmpValue.GetString());
                    }
                    tmpValue = jsonObject.GetNamedValue("TokenType");
                    if (tmpValue.ValueType == JsonValueType.String)
                    {
                        DebugPrint("TokenType = " + tmpValue.GetString());
                    }
                    tmpValue = jsonObject.GetNamedValue("CharacterOwnerHash");
                    if (tmpValue.ValueType == JsonValueType.String)
                    {
                        DebugPrint("CharacterOwnerHash = " + tmpValue.GetString());
                    }
                }
            }
        }

        private async void GetStats_Click(object sender, RoutedEventArgs e)
        {
            string EveUrl = "https://api-sisi.testeveonline.com";

            HttpClient httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Authorization = new HttpCredentialsHeaderValue(token_type, access_token);

            string response = await httpClient.GetStringAsync(new Uri(EveUrl));            
            JsonValue jsonValue = JsonValue.Parse(response);
            JsonObject jsonObject = jsonValue.GetObject();
            JsonValue tmpValue = jsonObject.GetNamedValue("decode");
            JsonValue href = tmpValue.GetObject().GetNamedValue("href");
            DebugPrint("decode URL = " + href.GetString());

            response = await httpClient.GetStringAsync(new Uri(href.GetString()));
            jsonValue = JsonValue.Parse(response);
            jsonObject = jsonValue.GetObject();
            tmpValue = jsonObject.GetNamedValue("character");
            href = tmpValue.GetObject().GetNamedValue("href");
            DebugPrint("character URL = " + href.GetString());

            response = await httpClient.GetStringAsync(new Uri(href.GetString()));
            jsonValue = JsonValue.Parse(response);
            jsonObject = jsonValue.GetObject();
            tmpValue = jsonObject.GetNamedValue("statistics");
            href = tmpValue.GetObject().GetNamedValue("href");
            DebugPrint("statistics URL = " + href.GetString());

            response = await httpClient.GetStringAsync(new Uri(href.GetString()));
            jsonValue = JsonValue.Parse(response);
            jsonObject = jsonValue.GetObject();
            tmpValue = jsonObject.GetNamedValue("items");
            foreach(var value in tmpValue.GetArray())
            {
                href = value.GetObject().GetNamedValue("href");
                DebugPrint("statistics URL = " + href.GetString());

                response = await httpClient.GetStringAsync(new Uri(href.GetString()));
                jsonValue = JsonValue.Parse(response);
                jsonObject = jsonValue.GetObject();
            }

        }
    }
}
