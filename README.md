This is a quick hack thrown together for experimenting with the Eve Online SSO (OAuth) service. It works on both Windows 8 (full screen app) and Windows Phone 8.1. 

The code is based on the web authentication sample from Microsoft which showcases the use of the WebAuthenticationBroker class. See http://code.msdn.microsoft.com/windowsapps/Web-Authentication-d0485122 for details about this sample code.

The callback URL you register on https://developers.testeveonline.com/ should equal the output of WebAuthenticationBroker.GetCurrentApplicationCallbackUri(). You will need to run the program once to get this key as it is unique to your build environment. 
